package com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.settings

import com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.ui.AppSettingsUI
import com.intellij.openapi.options.Configurable
import org.jetbrains.annotations.Nls
import org.jetbrains.annotations.Nullable
import javax.swing.JComponent

class AppSettingsConfigurable : Configurable {
    private var settingsUI: AppSettingsUI? = null
    private val settings: AppSettingsState = AppSettingsState.instance

    @Nls(capitalization = Nls.Capitalization.Title)
    override fun getDisplayName(): String {
        return "Swaggo Plugin Configuration"
    }

    @Nullable
    override fun createComponent(): JComponent? {
        settingsUI = AppSettingsUI()
        settingsUI!!.swagCliPath = settings.cliLocation
        settingsUI!!.docsOutputPath = settings.outputLocation
        settingsUI!!.cmdFormat = settings.fmtCmd
        settingsUI!!.cmdGenerate = settings.initCmd
        settingsUI!!.flagGeneralInfo = settings.generalInfoFlag
        settingsUI!!.flagDirectory = settings.directoryFlag
        settingsUI!!.flagOutput = settings.outputFlag
        return settingsUI!!.panel
    }

    override fun isModified(): Boolean {
        return (settingsUI!!.swagCliPath != settings.cliLocation ||
                settingsUI!!.docsOutputPath != settings.outputLocation ||
                settingsUI!!.cmdFormat != settings.fmtCmd ||
                settingsUI!!.cmdGenerate != settings.initCmd ||
                settingsUI!!.flagGeneralInfo != settings.generalInfoFlag ||
                settingsUI!!.flagDirectory != settings.directoryFlag ||
                settingsUI!!.flagOutput != settings.outputFlag
                )
    }

    override fun apply() {
        val settings: AppSettingsState = AppSettingsState.instance
        settings.cliLocation = settingsUI!!.swagCliPath
        settings.outputLocation = settingsUI!!.docsOutputPath
        settings.fmtCmd = settingsUI!!.cmdFormat
        settings.initCmd = settingsUI!!.cmdGenerate
        settings.generalInfoFlag = settingsUI!!.flagGeneralInfo
        settings.directoryFlag = settingsUI!!.flagDirectory
        settings.outputFlag = settingsUI!!.flagOutput
    }

    override fun disposeUIResources() {
        settingsUI = null
    }
}