package com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.actions

import com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.settings.AppSettingsState
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys.VIRTUAL_FILE
import com.intellij.openapi.vfs.VfsUtil

class SwagInitAction : AnAction() {
    private val settings = AppSettingsState.instance
    override fun actionPerformed(e: AnActionEvent) {
        e.project?.let {
            val output = settings.outputLocation.replace("./", "${it.basePath}/" ?: "./")

            Runtime.getRuntime()
                .exec(
                    arrayOf(
                        settings.cliLocation,
                        settings.initCmd,
                        settings.directoryFlag,
                        it.basePath,
                        settings.outputFlag,
                        output
                    )
                ).waitFor()

            val vf = e.getData(VIRTUAL_FILE) ?: return
            VfsUtil.markDirtyAndRefresh(false, true, true, vf)
        }
    }
}