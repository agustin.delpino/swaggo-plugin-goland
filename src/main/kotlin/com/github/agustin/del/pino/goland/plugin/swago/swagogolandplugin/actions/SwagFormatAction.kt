package com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.actions

import com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.settings.AppSettingsState
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys.EDITOR
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.util.io.FileUtil

class SwagFormatAction : AnAction() {
    private val settings = AppSettingsState.instance
    override fun actionPerformed(e: AnActionEvent) {
        ApplicationManager.getApplication().runWriteAction {
            val tempFile = FileUtil.createTempFile("__temp__swagger_comments", ".go")
            val editor = e.project?.let { FileEditorManager.getInstance(it) }

            val text = editor?.selectedTextEditor?.document?.text ?: return@runWriteAction

            tempFile.writeText(text)

            val c = arrayOf(settings.cliLocation, settings.fmtCmd, settings.directoryFlag, tempFile.absolutePath)
            Runtime.getRuntime().exec(c).waitFor()

            e.getData(EDITOR)?.document?.setText(tempFile.readText())
            tempFile.delete()
        }
    }

}