package com.github.agustin.del.pino.goland.plugin.swago.swagogolandplugin.ui

import com.intellij.icons.AllIcons

import com.intellij.openapi.fileChooser.FileChooserDescriptor
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBLabel
import com.intellij.ui.components.JBTextField
import com.intellij.util.ui.FormBuilder
import org.jetbrains.annotations.NotNull
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JPanel


class AppSettingsUI {
    var panel: JPanel? = null
        private set

    private val cliLocation = TextFieldWithBrowseButton()
    private val outputLocation = TextFieldWithBrowseButton()
    private val intiCmd = JBTextField()
    private val fmtCmd = JBTextField()
    private val generalInfoFlag = JBTextField()
    private val directoryFlag = JBTextField()
    private val outputFlag = JBTextField()


    private val fileChooser: FileChooserDescriptor = FileChooserDescriptorFactory.createSingleLocalFileDescriptor()
    private val dirChooser: FileChooserDescriptor = FileChooserDescriptorFactory.createSingleLocalFileDescriptor()

    init {

        cliLocation.addBrowseFolderListener(
            "Select the Swag CLI", null,
            ProjectManager.getInstance().defaultProject, fileChooser
        )

        outputLocation.addBrowseFolderListener(
            "Select the folder where the docs will be located", null,
            ProjectManager.getInstance().defaultProject, dirChooser
        )

        panel = FormBuilder.createFormBuilder()
            .addLabeledComponent(JBLabel("Swag CLI path: "), cliLocation, 5, false)
            .addLabeledComponent(JBLabel("Docs directory: "), outputLocation, 5, false)
            .addLabeledComponent(JBLabel("Generate command: "), intiCmd, 5, false)
            .addLabeledComponent(JBLabel("Format command: "), fmtCmd, 5, false)
            .addLabeledComponent(JBLabel("General Info flag: "), generalInfoFlag, 5, false)
            .addLabeledComponent(JBLabel("Scan directory flag: "), directoryFlag, 5, false)
            .addLabeledComponent(JBLabel("Output flag: "), outputFlag, 5, false)
            .addComponentFillVertically(JPanel(), 0)
            .panel
    }

    var swagCliPath: String
        get() {
            return cliLocation.text
        }
        set(value) {
            cliLocation.text = value
        }

    var docsOutputPath: String
        get() {
            return outputLocation.text
        }
        set(value) {
            outputLocation.text = value
        }

    var cmdGenerate: String
        get() {
            return intiCmd.text
        }
        set(value) {
            intiCmd.text = value
        }

    var cmdFormat: String
        get() {
            return fmtCmd.text
        }
        set(value) {
            fmtCmd.text = value
        }

    var flagGeneralInfo: String
        get() {
            return generalInfoFlag.text
        }
        set(value) {
            generalInfoFlag.text = value
        }

    var flagDirectory: String
        get() {
            return directoryFlag.text
        }
        set(value) {
            directoryFlag.text = value
        }

    var flagOutput: String
        get() {
            return outputFlag.text
        }
        set(value) {
            outputFlag.text = value
        }

}